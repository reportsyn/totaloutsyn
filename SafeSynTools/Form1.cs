﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;
using System.Diagnostics;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int cancel_count = Convert.ToInt32(ConfigurationManager.AppSettings["cancel_count"]);//获取实时同步的筆數
        int cancel_time = Convert.ToInt32(ConfigurationManager.AppSettings["cancel_time"]);//获取实时同步的执行间隔时间
        int cancel_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["cancel_waitX"]);//获取实时同步的执行间隔倍數
        int reward_count = Convert.ToInt32(ConfigurationManager.AppSettings["reward_count"]);//获取实时同步的筆數
        int reward_time = Convert.ToInt32(ConfigurationManager.AppSettings["reward_time"]);//获取实时同步的执行间隔时间
        int reward_waitX = Convert.ToInt32(ConfigurationManager.AppSettings["reward_waitX"]);//获取实时同步的执行间隔倍數
        string moveIdentityid = ConfigurationManager.AppSettings["moveIdentityid"].ToString();
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string mainConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_main"].ToString();//main
        string sql_totalOut = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalOut"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();
        System.Timers.Timer compareDelayTimer = new System.Timers.Timer();
        bool reportDelay = false;

        public Form1()
        {
            InitializeComponent();
            cancel_time = cancel_time * 1000;//秒
            reward_time = reward_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(synBySeconds));
            thread.Start();
            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();

            compareDelayTimer.Interval = 10000;
            compareDelayTimer.AutoReset = false;
            compareDelayTimer.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTime_Elapsed);
            compareDelayTimer.Start();
        }

        #region 实时同步的方法
        void synBySeconds()
        {
            Thread threadcancel = new Thread(new ThreadStart(syn_dt_lottery_cancellationsflow));
            threadcancel.Start();
            Thread threadreward = new Thread(new ThreadStart(syn_dt_reward_record));
            threadreward.Start();
        }

        void syn_dt_lottery_cancellationsflow()
        {
            string tablename = ConfigurationManager.AppSettings["onlyOne_cancel"].ToString();
            FillMsg2("正在同步...");
            //bool restart = false;
            int count = 0;
            int sleep_time = cancel_time;
            while (true)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    Thread.Sleep(1000 * 60 * 60 * 3);
                    continue;
                }
                //restart = false;
                count = 0;
                try
                {
                    //List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                    //    {
                    //        new SqlParameter("@tableName",tablename)
                    //    };
                    //string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                    //var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), sql_totalOut);
                    //byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    //List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    //SqlParameter Parameter_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    //Parameter_lockid.Value = lockid;
                    //AzureSqlParamter.Add(Parameter_lockid);
                    //List<string> maxLockid_list = new List<string>();
                    //string str = "select top " + cancel_count + " id,identityid,user_id,user_name,record_code,lotterycode,revocation_money,betting_id,scheme_id,flow_id,scheme_type,state,add_time,SourceName,lockid from " + tablename + " where lockid>@lockid and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=0) order by lockid asc";
                    string str = "select top " + cancel_count + " id,identityid,user_id,lotterycode,revocation_money,add_time,SourceName from " + tablename + " where reportSyn=0 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=0) order by id";
                    var table = SqlDbHelper.GetQuery(str, (reportDelay == true ? mainConnStr : reportConnStr));
                    if (table == null)
                        continue;
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        //List<string> list_update = new List<string>();
                        //for (int i = table.Rows.Count - 1; i >= 0; i--)
                        //{
                        //    byte[] a = (byte[])table.Rows[i]["lockid"];
                        //    string lockid_list = BitConverter.ToString(a).Replace("-", "");
                        //    maxLockid_list.Add(lockid_list);
                        //}
                        //string maxlockid = "0x" + maxLockid_list.Max();
                        //table.Columns.Remove("lockid");
                        List<SqlParameter> SqlParamter = new List<SqlParameter>();
                        TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                        int result = SqlDbHelper.RunInsert(table, "isVerify", "dsp_cancellationsflow_synSum",sql_totalOut);
                        TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = ts1.Subtract(ts2).Duration();
                        string dateDiff = ts.Seconds.ToString() + "秒";
                        FillMsg2(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                        WriteLogData(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff + "    同步汇总时间:" + DateTime.Now.ToString());

                        table.Columns.Remove("identityid");
                        table.Columns.Remove("user_id");
                        table.Columns.Remove("lotterycode");
                        table.Columns.Remove("revocation_money");
                        table.Columns.Remove("add_time");
                        table.Columns.Remove("SourceName");
                        SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                        Parameter_id.Value = table;
                        Parameter_id.TypeName = "report_id_type";
                        SqlParamter.Add(Parameter_id);

                        if (result == 0 )
                        {
                            TimeSpan tsU1 = new TimeSpan(DateTime.Now.Ticks);
                            SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 from dt_lottery_cancellationsflow with(index(index_id)) where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                            TimeSpan tsU2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan tsU = tsU1.Subtract(tsU2).Duration();
                            string dateDiffU = tsU.Seconds.ToString() + "秒";
                            WriteLogData("主表 結束更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                            FillMsg2("成功更新主表" + count + "条数据,耗时:" + dateDiffU);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == cancel_count)
                {
                    sleep_time = 2000;
                }
                else if (count >= cancel_count / 3 * 2)
                {
                    sleep_time = cancel_time;
                }
                else if (count < cancel_count / 3 * 2 && count >= cancel_count / 3)
                {
                    sleep_time = cancel_time * cancel_waitX;
                }
                else if (count < cancel_count / 3)
                {
                    sleep_time = cancel_time * (cancel_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        void syn_dt_reward_record()
        {
            string tablename = ConfigurationManager.AppSettings["onlyOne_reward"].ToString();
            FillMsg3("正在同步...");
            int count = 0;
            int sleep_time = reward_time;
            while (true)
            {
                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                {
                    Thread.Sleep(1000 * 60 * 60 * 3);
                    continue;
                }
                count = 0;
                try
                {
                    //List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                    //    {
                    //        new SqlParameter("@tableName",tablename)
                    //    };
                    //string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                    //var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), sql_totalOut);
                    //byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                    //List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                    //SqlParameter Parameter_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                    //Parameter_lockid.Value = lockid;
                    //AzureSqlParamter.Add(Parameter_lockid);
                    //List<string> maxLockid_list = new List<string>();
                    //string str = "select top " + reward_count + " id,user_id,identityid,anchor_id,gift_name,gift_price,combo,add_time,lockid from " + tablename + " where lockid>@lockid and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=0) and state=1 and datediff(second,update_time,DATEADD(hour, 8, getdate()))>10 order by lockid asc";
                    string str = "select top " + reward_count + " id,user_id,identityid,anchor_id,gift_name,gift_price,combo,add_time from " + tablename + " with(index(index_reportSyn)) where reportSyn=0 and " + (moveIdentityid.Length > 0 ? "identityid not in (" + moveIdentityid + ") and " : "") + "user_id in(select id from dt_users where flog=0) and state=1 and datediff(second,update_time,DATEADD(hour, 8, getdate()))>10 order by id";
                    var table = SqlDbHelper.GetQuery(str, (reportDelay == true ? mainConnStr : reportConnStr));
                    if (table == null)
                        continue;
                    count = table.Rows.Count;
                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                    {
                        List<SqlParameter> SqlParamter = new List<SqlParameter>();
                        //List<string> list_update = new List<string>();
                        //for (int i = table.Rows.Count - 1; i >= 0; i--)
                        //{
                        //    byte[] a = (byte[])table.Rows[i]["lockid"];
                        //    string lockid_list = BitConverter.ToString(a).Replace("-", "");
                        //    maxLockid_list.Add(lockid_list);
                        //}
                        //string maxlockid = "0x" + maxLockid_list.Max();
                        //table.Columns.Remove("lockid");
                        TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                        int result = SqlDbHelper.RunInsert(table, "isVerify", "dsp_reward_record_synSum", sql_totalOut);
                        TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = ts1.Subtract(ts2).Duration();
                        string dateDiff = ts.Seconds.ToString() + "秒";
                        FillMsg3(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff);
                        WriteLogData(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff + "    同步汇总时间:" + DateTime.Now.ToString());

                        table.Columns.Remove("user_id");
                        table.Columns.Remove("identityid");
                        table.Columns.Remove("anchor_id");
                        table.Columns.Remove("gift_name");
                        table.Columns.Remove("gift_price");
                        table.Columns.Remove("combo");
                        table.Columns.Remove("add_time");
                        SqlParameter Parameter_id = new SqlParameter("@idTable", SqlDbType.Structured);
                        Parameter_id.Value = table;
                        Parameter_id.TypeName = "report_id_type";
                        SqlParamter.Add(Parameter_id);

                        if (result == 0)
                        {
                            TimeSpan tsU1 = new TimeSpan(DateTime.Now.Ticks);
                            SqlDbHelper.ExecuteNonQuery("update " + tablename + " set reportSyn=2 from dt_reward_record where id in (select id from @idTable)", SqlParamter.ToArray(), mainConnStr);
                            TimeSpan tsU2 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan tsU = tsU1.Subtract(tsU2).Duration();
                            string dateDiffU = tsU.Seconds.ToString() + "秒";
                            WriteLogData("主表 結束更新" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fffffff"));
                            FillMsg3("成功更新主表" + count + "条数据,耗时:" + dateDiffU);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message != "未将对象引用设置到对象的实例。")
                    {
                        FillErrorMsg(tablename + ":" + ex);
                        WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
                if (count == reward_count)
                {
                    sleep_time = 2000;
                }
                else if (count >= reward_count / 3 * 2)
                {
                    sleep_time = reward_time;
                }
                else if (count < reward_count / 3 * 2 && count >= reward_count / 3)
                {
                    sleep_time = reward_time * reward_waitX;
                }
                else if (count < reward_count / 3)
                {
                    sleep_time = reward_time * (reward_waitX + 2);
                }
                Thread.Sleep(sleep_time);//睡眠时间
            }
        }

        //void syn_dt_user_get_point_record()
        //{
        //    string tablename = ConfigurationManager.AppSettings["onlyOne_point"].ToString();
        //    FillMsg1("正在同步...");
        //    int count = 0;
        //    int sleep_time = points_time;
        //    while (true)
        //    {
        //        if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
        //        {
        //            Thread.Sleep(1000 * 60 * 60 * 3);
        //            continue;
        //        }
        //        count = 0;
        //        try
        //        {
        //            List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
        //                    {
        //                        new SqlParameter("@tableName",tablename)
        //                    };
        //            string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
        //            var locktable = SqlDbHelper.GetQueryFromLocalData(querystr, LocalSqlParamter.ToArray(),sql_totalIn);
        //            byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
        //            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
        //            SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
        //            pa_lockid.Value = lockid;
        //            AzureSqlParamter.Add(pa_lockid);

        //            List<string> maxLockid_list = new List<string>();
        //            string str = "select top " + points_count + " *  from " + tablename + " where lockid>@lockid and user_id in(select id from dt_users where flog=0)  order by lockid asc";
        //            var table = SqlDbHelper.GetQueryFromAzure(str, AzureSqlParamter.ToArray());
        //            count = table.Rows.Count;
        //            if (table.Rows.Count != 0)//有数据时更新到本地数据库中
        //            {
        //                for (int i = table.Rows.Count - 1; i >= 0; i--)
        //                {
        //                    byte[] a = (byte[])table.Rows[i]["lockid"];
        //                    string lockid_list = BitConverter.ToString(a).Replace("-", "");
        //                    maxLockid_list.Add(lockid_list);
        //                }
        //                string maxlockid = "0x" + maxLockid_list.Max();
        //                table.Columns.Remove("lockid");

        //                TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
        //                int result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_get_point_synSum", sql_totalIn);
        //                TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
        //                TimeSpan ts = ts1.Subtract(ts2).Duration();
        //                string dateDiff = ts.Seconds.ToString() + "秒";
        //                FillMsg1(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff);
        //                WriteLogData(tablename + "成功同步汇总" + count + "条数据,耗时:" + dateDiff + "     同步汇总时间:" + DateTime.Now.ToString());
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            if (ex.Message != "未将对象引用设置到对象的实例。")
        //            {
        //                FillErrorMsg(tablename + ":" + ex);
        //                WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
        //            }
        //        }
        //        if (count == points_count)
        //        {
        //            sleep_time = 2000;
        //        }
        //        else if (count >= points_count / 3 * 2)
        //        {
        //            sleep_time = points_time;
        //        }
        //        else if (count < points_count / 3 * 2 && count >= points_count / 3)
        //        {
        //            sleep_time = points_time * points_waitX;
        //        }
        //        else if (count < points_count / 3)
        //        {
        //            sleep_time = points_time * (points_waitX + 2);
        //        }
        //        Thread.Sleep(sleep_time);//睡眠时间
        //    }
        //}

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBox3(string msg);
        private void FillMsg3(string msg)
        {
            if (richTextBox3.InvokeRequired)
            {
                RichBox3 rb = new RichBox3(FillMsg3);
                richTextBox3.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox3.IsHandleCreated)
                {
                    richTextBox3.AppendText(msg);
                    richTextBox3.AppendText("\r\n");
                    richTextBox3.SelectionStart = richTextBox3.Text.Length;
                    richTextBox3.SelectionLength = 0;
                    richTextBox3.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\t\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 打印错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "10")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & richTextBox3.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                richTextBox3.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    richTextBox3.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        private void compareSQLTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeMain;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, mainConnStr);
                timeMain = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, reportConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeMain.Subtract(timeReport).TotalSeconds >= 60)
                {
                    //delaySecond = Convert.ToInt32(timeMain.Subtract(timeReport).TotalSeconds);
                    if (reportDelay == false)
                    {
                        reportDelay = true;
                        WriteErrorLog("--------------------------\r\n報表從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    //delaySecond = 0;
                    if (reportDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n報表從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    reportDelay = false;
                }
            }
            catch (Exception ex)
            {
                reportDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }
    }
}
